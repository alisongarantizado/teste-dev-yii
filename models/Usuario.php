<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property integer $usua_codigo
 * @property string $usua_nome
 * @property string $usua_email
 * @property string $usua_senha
 * @property boolean $usua_habilitado
 *
 * @property Produto[] $produtos
 */
class Usuario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usua_nome', 'usua_email', 'usua_senha', 'usua_habilitado'], 'required'],
            [['usua_habilitado'], 'boolean'],
            [['usua_nome'], 'string', 'max' => 60],
            [['usua_email'], 'string', 'max' => 40],
            [['usua_senha'], 'string', 'max' => 600]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'usua_codigo' => 'Codigo',
            'usua_nome' => 'Nome',
            'usua_email' => 'Email',
            'usua_senha' => 'Senha',
            'usua_habilitado' => 'Habilitado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProdutos()
    {
        return $this->hasMany(Produto::className(), ['usua_codigo' => 'usua_codigo']);
    }
}
