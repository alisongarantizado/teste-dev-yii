<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Produto;

/**
 * ProdutoSearch represents the model behind the search form about `app\models\Produto`.
 */
class ProdutoSearch extends Produto
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prod_codigo', 'marc_codigo', 'prod_quantidade', 'usua_codigo'], 'integer'],
            [['prod_nome'], 'safe'],
            [['prod_preco'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Produto::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
        #    'prod_codigo' => $this->prod_codigo,
        #   'marc_codigo' => $this->marc_codigo,
           #'prod_preco' => $this->prod_preco,
        #    'prod_quantidade' => $this->prod_quantidade,
        #    'usua_codigo' => $this->usua_codigo,
        ]);
        
        $query->andFilterWhere(['like', 'prod_nome', $this->prod_nome]);
        $query->andFilterWhere(['like', 'cast(prod_preco as text)', $this->prod_preco]);
        $query->andFilterWhere(['like', 'cast(prod_codigo as text)', $this->prod_codigo]);
        $query->andFilterWhere(['like', 'cast(marc_codigo as text)', $this->marc_codigo]);
        $query->andFilterWhere(['like', 'cast(prod_quantidade as text)', $this->prod_quantidade]);
        $query->andFilterWhere(['like', 'cast(usua_codigo as text)', $this->usua_codigo]);

        return $dataProvider;
    }
}
