<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "produto".
 *
 * @property integer $prod_codigo
 * @property string $prod_nome
 * @property integer $marc_codigo
 * @property double $prod_preco
 * @property integer $prod_quantidade
 * @property integer $usua_codigo
 *
 * @property Marca $marcCodigo
 * @property Usuario $usuaCodigo
 */
class Produto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'produto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prod_nome', 'marc_codigo', 'prod_preco', 'prod_quantidade', 'usua_codigo'], 'required'],
            [['marc_codigo', 'prod_quantidade', 'usua_codigo'], 'integer'],
            [['prod_preco'], 'number'],
            [['prod_nome'], 'string', 'max' => 60]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'prod_codigo' => 'Codigo',
            'prod_nome' => ' Nome',
            'marc_codigo' => 'Codigo da Marca',
            'prod_preco' => ' Preco',
            'prod_quantidade' => 'Quantidade',
            'usua_codigo' => 'Codigo do Usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarcCodigo()
    {
        return $this->hasOne(Marca::className(), ['marc_codigo' => 'marc_codigo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuaCodigo()
    {
        return $this->hasOne(Usuario::className(), ['usua_codigo' => 'usua_codigo']);
    }
}
