<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProdutoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Produtos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="produto-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);
      ?>

    
     <table border="0", align="lefth">
<tr> 
    <td><?= $this->render('_search', ['model' => $searchModel])?></td>
    
</tr>
    
   <table border="0", align="right">
<tr> 
    <td><?= Html::a('Novo Produto', ['create'], ['class' => 'btn btn-success']) ?></td>
    
</tr> 
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
#        'filterModel' => $searchModel,
        'columns' => [
 #           ['class' => 'yii\grid\SerialColumn'],

            'prod_codigo',
            'prod_nome',
            'marc_codigo',
            'prod_preco',
            'prod_quantidade',
            // 'usua_codigo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
