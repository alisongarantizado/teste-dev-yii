<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Produto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="input-group input-group-lg ">

    <?php $form = ActiveForm::begin(); 
    ?>

    <?= $form->field($model, 'prod_nome')->textInput(['maxlength' => 60]) ?>

    <?= $form->field($model, 'marc_codigo')->dropDownList(ArrayHelper::map(app\models\MarcaSearch::find()->all(), 'marc_codigo', 'marc_codigo')) ?>

    <?= $form->field($model, 'prod_preco')->textInput() ?>

    <?= $form->field($model, 'prod_quantidade')->textInput() ?>
    
    <?= $form->field($model, 'usua_codigo')->dropDownList(ArrayHelper::map(app\models\UsuarioSearch::find()->all(), 'usua_codigo', 'usua_codigo')) ?>

    </div>
    <p>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Criar' : 'Salvar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</p>
    <?php ActiveForm::end(); ?>


