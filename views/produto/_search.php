<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProdutoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="produto-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
<table border="0", align="lefth">
<tr> 
    <td><?= $form->field($model, 'prod_codigo') ?></td>

    <td><?= $form->field($model, 'prod_nome') ?></td>

    <td><?= $form->field($model, 'marc_codigo') ?></td>

    <td><?= $form->field($model, 'prod_preco') ?></td>

    <td><?= $form->field($model, 'prod_quantidade') ?></td>

    <?php // echo $form->field($model, 'usua_codigo') ?>

    <div class="form-group">
        <td><?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?></td>
    </div>
    <tr>

    <?php ActiveForm::end(); ?>

</div>
