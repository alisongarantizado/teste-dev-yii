<?php
/* @var $this yii\web\View */
use yii\bootstrap\Carousel;
$this->title = 'My Yii Application';

?>
<div class="site-index">

   
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active"align="middle">
       <a href=".?r=produto/index"> <img src="../imgs/produtos.png"></a>
      <div class="carousel-caption">
          <font color="green"> <h1>Produtos</h1></font>
      </div>
    </div>
    <div class="item"align="middle">
       
      <a href=".?r=marca/index"> <img src="../imgs/marcas.png"></a>
            <br><br>  <br><br>  <br><br>

      <div class="carousel-caption">
          <font color="blue"> <h1>Marcas</h1></font>
      </div>
    </div>
      
       <div class="item"align="middle">    
      <a href=".?r=usuario/index"> <img src="../imgs/users.png", align="middle"></a>
      <br><br>  <br><br>  <br><br>
      <div class="carousel-caption" align="center">
          <font color="orange"> <h1>Usuários</h1></font>
      </div>
    </div>
   
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>
    
    
</div>
