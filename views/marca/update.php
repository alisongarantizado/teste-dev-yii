<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Marca */

$this->title = 'Editar Marca: ' . ' ' . $model->marc_nome;
$this->params['breadcrumbs'][] = ['label' => 'Marcas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->marc_nome, 'url' => ['view', 'id' => $model->marc_codigo]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="marca-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
     <table border="0", align="right">
<tr> 
    <td></td>
    
</tr>
</div>
