<?php
use yii\widgets\DetailView;

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\Standard;
use yii\widgets\Menu;
 
/* @var $this yii\web\View */
/* @var $searchModel app\models\MarcaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Marcas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marca-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php 
//        'filterModel' => Html::activeDropDownList($searchModel, 'marc_codigo', ArrayHelper::map($searchModel::find()->all(), 'marc_codigo', 'marc_codigo')),

   // echo $this->render('_search', ['model' => $searchModel]); 
    
    ?>
    

      <table border="0", align="lefth">
<tr> 
    <td><?= $this->render('_search', ['model' => $searchModel])?></td>
    
</tr>

<table border="0", align="right">
<tr> 
    <td><?= Html::a('Nova Marca', ['create'], ['class' => 'btn btn-success']) ?></td>
    
</tr> 


<?= GridView::widget([
        'dataProvider' => $dataProvider,
        #'filterModel' => $searchModel,
        'columns' => [
             
            'marc_codigo',
            'marc_nome',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
       
  
 
 
</div>
