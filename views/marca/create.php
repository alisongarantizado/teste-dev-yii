<?php
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Marca */

$this->title = 'Nova Marca';
$this->params['breadcrumbs'][] = ['label' => 'Marcas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="input-group input-group-lg">

    
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
     <table border="0", align="right">
<tr> 
    <td></td>
    
</tr>
</div>
