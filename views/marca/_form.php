<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Marca */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="input-group input-group-lg">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'marc_nome')->textInput(['maxlength' => 60]) ?>

</div>
    <p>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Criar' : 'Salvar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</p>
    <?php ActiveForm::end(); ?>

