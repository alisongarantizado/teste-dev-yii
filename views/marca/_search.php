<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MarcaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="marca-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        
    ]); ?>

    <div class="form-group">
    
      <table border="0", align="lefth">
<tr> 
    <td><?=$form->field($model, 'marc_codigo')?> </td>
    <td><?=$form->field($model, 'marc_nome')?> </td>
    <td><?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?></td>
</tr>

    </div>

    <?php ActiveForm::end(); ?>

</div>
