<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="input-group input-group-lg">

    <?php $form = ActiveForm::begin(); ?>



    <?= $form->field($model, 'usua_nome')->textInput(['maxlength' => 60]) ?>

    <?= $form->field($model, 'usua_email')->textInput(['maxlength' => 40]) ?>

    <?= $form->field($model, 'usua_senha')->passwordInput(['maxlength' => 600]) ?>

  <?= $form->field($model, 'usua_habilitado')->dropDownList(['No','Yes']) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Criar' : 'Salvar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
