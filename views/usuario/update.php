<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */

    $this->title = 'Editar Usuario: ' . ' ' . $model->usua_nome;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->usua_nome, 'url' => ['view', 'id' => $model->usua_codigo]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="usuario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
     <table border="0", align="right">
<tr> 
    <td></td>
    
</tr>
</div>
