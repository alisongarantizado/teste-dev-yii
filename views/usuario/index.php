<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsuarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    
     <table border="0", align="lefth">
<tr> 
    <td><?= $this->render('_search', ['model' => $searchModel])?></td>
    
</tr>
    


  <table border="0", align="right">
<tr> 
    <td><?= Html::a('Novo Usuario', ['create'], ['class' => 'btn btn-success']) ?></td>
    
</tr> 


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
#        'filterModel' => $searchModel,
        'columns' => [
 #           ['class' => 'yii\grid\SerialColumn'],

            'usua_codigo',
            'usua_nome',
            'usua_email:email',
            'usua_senha',
            'usua_habilitado:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
