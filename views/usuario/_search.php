<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UsuarioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuario-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
<table border="0", align="lefth">
<tr> 
 <td>  <?= $form->field($model, 'usua_codigo') ?></td>

 <td>   <?= $form->field($model, 'usua_nome') ?></td>

 <td>   <?= $form->field($model, 'usua_email') ?></td>

<td>    <?= $form->field($model, 'usua_senha') ?></td>

 <td>   <?= $form->field($model, 'usua_habilitado')->dropDownList(['No','Yes']) ?></td>
 <td><?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?></td>

</tr>
   
    <?php ActiveForm::end(); ?>

</div>
